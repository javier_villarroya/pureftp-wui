<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140527172238 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE users DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE users ADD id INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST");
        $this->addSql("ALTER TABLE admin DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE admin ADD id INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE admin DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE admin DROP id, CHANGE Username Username VARCHAR(35) DEFAULT '' NOT NULL, CHANGE Password Password CHAR(32) DEFAULT '' NOT NULL");
        $this->addSql("ALTER TABLE admin ADD PRIMARY KEY (Username)");
        $this->addSql("ALTER TABLE users DROP PRIMARY KEY");
        $this->addSql("ALTER TABLE users DROP id, CHANGE User User VARCHAR(16) DEFAULT '' NOT NULL, CHANGE Password Password VARCHAR(32) DEFAULT '' NOT NULL, CHANGE Uid Uid INT DEFAULT 14 NOT NULL, CHANGE Gid Gid INT DEFAULT 5 NOT NULL, CHANGE Dir Dir VARCHAR(128) DEFAULT '' NOT NULL, CHANGE QuotaFiles QuotaFiles INT DEFAULT 500 NOT NULL, CHANGE QuotaSize QuotaSize INT DEFAULT 30 NOT NULL, CHANGE ULBandwidth ULBandwidth INT DEFAULT 80 NOT NULL, CHANGE DLBandwidth DLBandwidth INT DEFAULT 80 NOT NULL, CHANGE Ipaddress Ipaddress VARCHAR(15) DEFAULT '*' NOT NULL, CHANGE Status Status VARCHAR(255) DEFAULT '1' NOT NULL, CHANGE ULRatio ULRatio SMALLINT DEFAULT '1' NOT NULL, CHANGE DLRatio DLRatio SMALLINT DEFAULT '1' NOT NULL");
        $this->addSql("ALTER TABLE users ADD PRIMARY KEY (User)");
    }
}
