<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140528085911 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("ALTER TABLE users CHANGE User User VARCHAR(16) NOT NULL, CHANGE Password Password VARCHAR(32) NOT NULL, CHANGE Uid Uid INT NOT NULL, CHANGE Gid Gid INT NOT NULL, CHANGE Dir Dir VARCHAR(128) NOT NULL, CHANGE QuotaFiles QuotaFiles INT NOT NULL, CHANGE QuotaSize QuotaSize INT NOT NULL, CHANGE ULBandwidth ULBandwidth INT NOT NULL, CHANGE DLBandwidth DLBandwidth INT NOT NULL, CHANGE Ipaddress Ipaddress VARCHAR(15) NOT NULL, CHANGE Status Status VARCHAR(255) NOT NULL, CHANGE ULRatio ULRatio SMALLINT NOT NULL, CHANGE DLRatio DLRatio SMALLINT NOT NULL");
        $this->addSql("ALTER TABLE admin ADD username_canonical VARCHAR(255) NOT NULL, ADD email VARCHAR(255) NOT NULL, ADD email_canonical VARCHAR(255) NOT NULL, ADD enabled TINYINT(1) NOT NULL, ADD salt VARCHAR(255) NOT NULL, ADD last_login DATETIME DEFAULT NULL, ADD locked TINYINT(1) NOT NULL, ADD expired TINYINT(1) NOT NULL, ADD expires_at DATETIME DEFAULT NULL, ADD confirmation_token VARCHAR(255) DEFAULT NULL, ADD password_requested_at DATETIME DEFAULT NULL, ADD roles LONGTEXT NOT NULL COMMENT '(DC2Type:array)', ADD credentials_expired TINYINT(1) NOT NULL, ADD credentials_expire_at DATETIME DEFAULT NULL, CHANGE Username username VARCHAR(255) NOT NULL, CHANGE Password password VARCHAR(255) NOT NULL");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_880E0D7692FC23A8 ON admin (username_canonical)");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_880E0D76A0D96FBF ON admin (email_canonical)");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql", "Migration can only be executed safely on 'mysql'.");
        
        $this->addSql("DROP INDEX UNIQ_880E0D7692FC23A8 ON admin");
        $this->addSql("DROP INDEX UNIQ_880E0D76A0D96FBF ON admin");
        $this->addSql("ALTER TABLE admin DROP username_canonical, DROP email, DROP email_canonical, DROP enabled, DROP salt, DROP last_login, DROP locked, DROP expired, DROP expires_at, DROP confirmation_token, DROP password_requested_at, DROP roles, DROP credentials_expired, DROP credentials_expire_at, CHANGE username Username VARCHAR(35) DEFAULT '' NOT NULL, CHANGE password Password CHAR(32) DEFAULT '' NOT NULL");
        $this->addSql("ALTER TABLE users CHANGE User User VARCHAR(16) DEFAULT '' NOT NULL, CHANGE Password Password VARCHAR(32) DEFAULT '' NOT NULL, CHANGE Uid Uid INT DEFAULT 14 NOT NULL, CHANGE Gid Gid INT DEFAULT 5 NOT NULL, CHANGE Dir Dir VARCHAR(128) DEFAULT '' NOT NULL, CHANGE QuotaFiles QuotaFiles INT DEFAULT 500 NOT NULL, CHANGE QuotaSize QuotaSize INT DEFAULT 30 NOT NULL, CHANGE ULBandwidth ULBandwidth INT DEFAULT 80 NOT NULL, CHANGE DLBandwidth DLBandwidth INT DEFAULT 80 NOT NULL, CHANGE Ipaddress Ipaddress VARCHAR(15) DEFAULT '*' NOT NULL, CHANGE Status Status VARCHAR(255) DEFAULT '1' NOT NULL, CHANGE ULRatio ULRatio SMALLINT DEFAULT '1' NOT NULL, CHANGE DLRatio DLRatio SMALLINT DEFAULT '1' NOT NULL");
    }
}
