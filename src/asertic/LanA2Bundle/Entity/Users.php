<?php

namespace asertic\LanA2Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 */
class Users
{
    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var integer
     */
    private $uid;

    /**
     * @var integer
     */
    private $gid;

    /**
     * @var string
     */
    private $dir;

    /**
     * @var integer
     */
    private $quotafiles;

    /**
     * @var integer
     */
    private $quotasize;

    /**
     * @var integer
     */
    private $ulbandwidth;

    /**
     * @var integer
     */
    private $dlbandwidth;

    /**
     * @var string
     */
    private $ipaddress;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $status;

    /**
     * @var integer
     */
    private $ulratio;

    /**
     * @var integer
     */
    private $dlratio;

    /**
     * @var string
     */
    private $domain;


    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Users
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set uid
     *
     * @param integer $uid
     * @return Users
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return integer 
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set gid
     *
     * @param integer $gid
     * @return Users
     */
    public function setGid($gid)
    {
        $this->gid = $gid;

        return $this;
    }

    /**
     * Get gid
     *
     * @return integer 
     */
    public function getGid()
    {
        return $this->gid;
    }

    /**
     * Set dir
     *
     * @param string $dir
     * @return Users
     */
    public function setDir($dir)
    {
        $this->dir = $dir;

        return $this;
    }

    /**
     * Get dir
     *
     * @return string 
     */
    public function getDir()
    {
        return $this->dir;
    }

    /**
     * Set quotafiles
     *
     * @param integer $quotafiles
     * @return Users
     */
    public function setQuotafiles($quotafiles)
    {
        $this->quotafiles = $quotafiles;

        return $this;
    }

    /**
     * Get quotafiles
     *
     * @return integer 
     */
    public function getQuotafiles()
    {
        return $this->quotafiles;
    }

    /**
     * Set quotasize
     *
     * @param integer $quotasize
     * @return Users
     */
    public function setQuotasize($quotasize)
    {
        $this->quotasize = $quotasize;

        return $this;
    }

    /**
     * Get quotasize
     *
     * @return integer 
     */
    public function getQuotasize()
    {
        return $this->quotasize;
    }

    /**
     * Set ulbandwidth
     *
     * @param integer $ulbandwidth
     * @return Users
     */
    public function setUlbandwidth($ulbandwidth)
    {
        $this->ulbandwidth = $ulbandwidth;

        return $this;
    }

    /**
     * Get ulbandwidth
     *
     * @return integer 
     */
    public function getUlbandwidth()
    {
        return $this->ulbandwidth;
    }

    /**
     * Set dlbandwidth
     *
     * @param integer $dlbandwidth
     * @return Users
     */
    public function setDlbandwidth($dlbandwidth)
    {
        $this->dlbandwidth = $dlbandwidth;

        return $this;
    }

    /**
     * Get dlbandwidth
     *
     * @return integer 
     */
    public function getDlbandwidth()
    {
        return $this->dlbandwidth;
    }

    /**
     * Set ipaddress
     *
     * @param string $ipaddress
     * @return Users
     */
    public function setIpaddress($ipaddress)
    {
        $this->ipaddress = $ipaddress;

        return $this;
    }

    /**
     * Get ipaddress
     *
     * @return string 
     */
    public function getIpaddress()
    {
        return $this->ipaddress;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Users
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Users
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set ulratio
     *
     * @param integer $ulratio
     * @return Users
     */
    public function setUlratio($ulratio)
    {
        $this->ulratio = $ulratio;

        return $this;
    }

    /**
     * Get ulratio
     *
     * @return integer 
     */
    public function getUlratio()
    {
        return $this->ulratio;
    }

    /**
     * Set dlratio
     *
     * @param integer $dlratio
     * @return Users
     */
    public function setDlratio($dlratio)
    {
        $this->dlratio = $dlratio;

        return $this;
    }

    /**
     * Get dlratio
     *
     * @return integer 
     */
    public function getDlratio()
    {
        return $this->dlratio;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Users
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set id
     *
     * @param string $id
     * @return Users
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get Id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @var integer
     */
    private $id;


    /**
     * Set user
     *
     * @param string $user
     * @return Users
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
    
    public function createDir($path)
    {
     if (!(file_exists($path))){
      if (!mkdir($path, 0777, TRUE)) {
       return false;
      }
     }
     return true;
    }
}
