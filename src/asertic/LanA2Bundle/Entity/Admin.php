<?php

namespace asertic\LanA2Bundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Admin
 */
class Admin extends BaseUser
{
    /**
     * @var string
     */
    private $domain;

    /**
     * @var string
     */
    private $baseDir;


    /**
     * Set domain
     *
     * @param string $domain
     * @return Admin
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return string 
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Set baseDir
     *
     * @param string $baseDir
     * @return Admin
     */
    public function setBaseDir($baseDir)
    {
        $this->baseDir = $baseDir;

        return $this;
    }

    /**
     * Get baseDir
     *
     * @return string 
     */
    public function getBaseDir()
    {
        return $this->baseDir;
    }
    /**
     * @var integer
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function isAdmin(){
     $roles = $this->getRoles();
    
     $isadmin = false;
    
     foreach ($roles as $rol){
      if ($rol == 'ROLE_ADMIN') {
       $isadmin = true;
       break;
      }
     }
     return $isadmin;
    }
    
    // ------------ lixlpixel recursive PHP functions -------------
    // recursive_directory_size( directory, human readable format )
    // expects path to directory and optional TRUE / FALSE
    // ------------------------------------------------------------
    public function recursive_directory_size($directory, $format=FALSE){
     $size = 0;
     if(substr($directory,-1) == '/'){
      $directory = substr($directory,0,-1);
     }
     if(!file_exists($directory) || !is_dir($directory) || !is_readable($directory))
     {
      return -1;
     }
     if($handle = opendir($directory))
     {
      while(($file = readdir($handle)) !== false)
      {
       $path = $directory.'/'.$file;
       if($file != '.' && $file != '..')
       {
        if(is_file($path))
        {
         $size += filesize($path);
        }elseif(is_dir($path))
        {
         $handlesize = $this->recursive_directory_size($path);
         if($handlesize >= 0)
         {
          $size += $handlesize;
         }else{
          return -1;
         }
        }
       }
      }
      closedir($handle);
     }
     if($format == TRUE)
     {
      if($size / 1048576 > 1)
      {
       return round($size / 1048576, 1).' MB';
      }elseif($size / 1024 > 1)
      {
       return round($size / 1024, 1).' KB';
      }else{
       return round($size, 1).' bytes';
      }
     }else{
      return $size;
     }
    }
    public function getDirectorySize(){
      return $this->recursive_directory_size($this->getBaseDir(), TRUE);
    }
    
}
