<?php

namespace asertic\LanA2Bundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use asertic\LanA2Bundle\Entity\Users;
use asertic\LanA2Bundle\Form\UsersType;

/**
 * Users controller.
 *
 */
class UsersController extends Controller
{

    /**
     * Lists all Users entities.
     *
     */
    public function indexAction()
    {
     if( $this->get('security.context')->getToken()->getUser() == 'anon.'){
       return $this->redirect($this->generateUrl('fos_user_security_login'));
     }

     $em = $this->getDoctrine()->getManager();
     
     if ($this->get('security.context')->getToken()->getUser()->isAdmin()){
      $entities = $em->getRepository('aserticLanA2Bundle:Users')->findBy(array(), array('domain' => 'ASC','user' => 'ASC'));
     } else {
      $entities = $em->getRepository('aserticLanA2Bundle:Users')->findBy(array('domain' => $this->get('security.context')->getToken()->getUser()->getDomain()), array('user' => 'ASC'));
     }

        return $this->render('aserticLanA2Bundle:Users:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Lists Users by domain
     *
     */
    public function indexsearchAction()
    {
     if( $this->get('security.context')->getToken()->getUser() == 'anon.'){
      return $this->redirect($this->generateUrl('fos_user_security_login'));
     }
    
     $em = $this->getDoctrine()->getManager();
      
     if ($this->get('security.context')->getToken()->getUser()->isAdmin()){
      $entities = $em->getRepository('aserticLanA2Bundle:Users')->findBy(array('domain' => $this->getRequest()->get('domain')), array('user' => 'ASC'));
      if (!$this->getRequest()->get('domain')){
       $entities = $em->getRepository('aserticLanA2Bundle:Users')->findBy(array(), array('domain' => 'ASC' , 'user' => 'ASC'));
      }
     } else {
      $entities = $em->getRepository('aserticLanA2Bundle:Users')->findBy(array('domain' => $this->get('security.context')->getToken()->getUser()->getDomain()), array('user' => 'ASC'));
     }
    
     return $this->render('aserticLanA2Bundle:Users:index.html.twig', array(
       'entities' => $entities,
     ));
    }    
    /**
     * Creates a new Users entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Users();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
         $form_data = $request->request->all()["asertic_lana2bundle_users"];

         if ($form_data['password']['first']) {
          $entity->setPassword(md5($form_data['password']['first']));
         }

         $homedir = $this->get('security.context')->getToken()->getUser()->getBaseDir() ; 
           if ($form_data['subdir']) {
            $homedir = str_replace('//','/',$homedir . '/' . $form_data['subdir']);
           }
           if ($entity->createDir($homedir)) {
            $entity->setDir($homedir);
           }         
        
        if (!$entity->getQuotafiles()){
         $entity->setQuotafiles($this->container->getParameter('quotafiles'));
        }
        if (!$entity->getQuotasize()){
         $entity->setQuotasize($this->container->getParameter('quotasize'));
        }
        if (!$entity->getUlbandwidth()){
         $entity->setUlbandwidth($this->container->getParameter('ulbandwidth'));
        }
        if (!$entity->getDlbandwidth()){
         $entity->setDlbandwidth($this->container->getParameter('dlbandwidth'));
        }
        if (!$entity->getDlratio()){
         $entity->setDlratio($this->container->getParameter('dlratio'));
        }
        if (!$entity->getUlratio()){        
         $entity->setUlratio($this->container->getParameter('ulratio'));
        }
        if (!$entity->getIpaddress()){        
         $entity->setIpaddress($this->container->getParameter('ipaddress'));
        }
        if (!$entity->getUid()){  
         $entity->setUid($this->container->getParameter('uid'));
        }
        if (!$entity->getGid()){
         $entity->setGid($this->container->getParameter('gid'));
        }
        if (!$entity->getStatus()){
         $entity->setStatus('1');
        }
           
           $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('users'));
        }

        return $this->render('aserticLanA2Bundle:Users:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Users entity.
    *
    * @param Users $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Users $entity)
    {
        $form = $this->createForm(new UsersType($this->get('security.context')), $entity, array(
            'action' => $this->generateUrl('users_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Users entity.
     *
     */
    public function newAction()
    {
        $entity = new Users();
        
        $entity->setDir($this->container->getParameter('ftp_root'));
        $entity->setQuotafiles($this->container->getParameter('quotafiles'));
        $entity->setQuotasize($this->container->getParameter('quotasize'));
        $entity->setUlbandwidth($this->container->getParameter('ulbandwidth'));
        $entity->setDlbandwidth($this->container->getParameter('dlbandwidth'));
        $entity->setDlratio($this->container->getParameter('dlratio'));
        $entity->setUlratio($this->container->getParameter('ulratio'));
        $entity->setIpaddress($this->container->getParameter('ipaddress'));
        $entity->setUid($this->container->getParameter('uid'));
        $entity->setGid($this->container->getParameter('gid'));
        $entity->setStatus('1');
        
        if (!($this->get('security.context')->getToken()->getUser()->isAdmin())) {
         $entity->setDomain($this->get('security.context')->getToken()->getUser()->getDomain());
         $entity->setDir($this->get('security.context')->getToken()->getUser()->getBaseDir());
        }

        $form   = $this->createCreateForm($entity);
          
        return $this->render('aserticLanA2Bundle:Users:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Users entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('aserticLanA2Bundle:Users')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Users entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('aserticLanA2Bundle:Users:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Users entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('aserticLanA2Bundle:Users')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Users entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('aserticLanA2Bundle:Users:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Users entity.
    *
    * @param Users $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Users $entity)
    {
        $form = $this->createForm(new UsersType($this->get('security.context')), $entity, array(
            'action' => $this->generateUrl('users_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Users entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('aserticLanA2Bundle:Users')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Users entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
           $form_data = $request->request->all()["asertic_lana2bundle_users"];
           
           if ($form_data['password']['first']) {
            $entity->setPassword(md5($form_data['password']['first']));
           }
           if (array_key_exists('subdir', $form_data) && $form_data['subdir']){
              $homedir = str_replace('//','/',$entity->getDir() . '/' . $form_data['subdir']);
              if (!is_dir($homedir)){
               if ($entity->createDir($homedir)) {
                $entity->setDir($homedir);
               }
              }
            }
           
              
           $em->flush();

           return $this->redirect($this->generateUrl('users_edit', array('id' => $id)));
        }

        return $this->render('aserticLanA2Bundle:Users:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Users entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('aserticLanA2Bundle:Users')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Users entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('users'));
    }

    /**
     * Creates a form to delete a Users entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('users_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
