<?php

namespace asertic\LanA2Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\SecurityContext;

class UsersType extends AbstractType
{
 private $securityContext;
 
 public function __construct(SecurityContext $securityContext)
 {
  $this->securityContext = $securityContext;
 }
 
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
     if ($this->securityContext->getToken()->getUser()->isAdmin()) {
      $builder
      ->add('user')
      ->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match.',
            'options' => array('attr' => array('class' => 'password-field')),
            'required' => true,
            'first_options'  => array('label' => 'Password'),
            'second_options' => array('label' => 'Repeat Password'),
      ))
      ->add('dir')
      ->add('subdir', 'text', array('mapped' => false, 'required' => false,))
      ->add('domain')
      ->add('comment')
      ->add('uid')
      ->add('gid')
      ->add('quotafiles')
      ->add('quotasize')
      ->add('ulbandwidth')
      ->add('dlbandwidth')
      ->add('ipaddress')
      ->add('ulratio')
      ->add('dlratio')
      ->add('status')
      ;
      
     } else {
      if ($options['action'] == '/users/create') {
       $builder
       ->add('user')
       ->add('password', 'repeated', array(
         'type' => 'password',
         'invalid_message' => 'The password fields must match.',
         'options' => array('attr' => array('class' => 'password-field')),
         'required' => false,
         'first_options'  => array('label' => 'Password'),
         'second_options' => array('label' => 'Repeat Password'),
       ))
       ->add('dir', 'text', array('read_only' => true,))
       ->add('subdir', 'text', array('mapped' => false, 'required' => false,))
       ->add('domain', 'text', array('read_only' => true,))
       ;
        
       
      }else{
       $builder
       ->add('user', 'text', array('read_only' => true,))
       ->add('password', 'repeated', array(
         'type' => 'password',
         'invalid_message' => 'The password fields must match.',
         'options' => array('attr' => array('class' => 'password-field')),
         'required' => false,
         'first_options'  => array('label' => 'Password'),
         'second_options' => array('label' => 'Repeat Password'),
       ))
       ->add('dir', 'text', array('read_only' => true,))
       ->add('domain', 'text', array('read_only' => true,))
       ;
        
      }
      
     }
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'asertic\LanA2Bundle\Entity\Users'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'asertic_lana2bundle_users';
    }
}
