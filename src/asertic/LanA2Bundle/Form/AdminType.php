<?php

namespace asertic\LanA2Bundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email',null, array('required' => false))
            ->add('plainPassword', 'repeated', array(
            	'type' => 'password',
            	'options' => array('translation_domain' => 'FOSUserBundle'),
            	'first_options' => array('label' => 'form.new_password'),
            	'second_options' => array('label' => 'form.new_password_confirmation'),
            	'invalid_message' => 'fos_user.password.mismatch',
      			'required' => false,
        	))
            ->add('domain')
            ->add('baseDir')
            ->add('enabled',null, array('required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'asertic\LanA2Bundle\Entity\Admin'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'asertic_lana2bundle_admin';
    }
}
